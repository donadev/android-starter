package it.luigidonadel;

/**
 * Created by donadev on 23/10/2018.
 */
//support for api < 24
public class Optional<T> {
    private T value;
    private Optional(T value) {
        this.value = value;
    }

    public static <T> Optional<T> from(T item) {
        return new Optional<>(item);
    }

    public Boolean hasValue() {
        return value != null;
    }
    public T get() {
        return value;
    }
}
