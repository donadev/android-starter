package it.luigidonadel.appcompat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by donadev on 10/10/2018.
 */

public abstract class LDAdapter<T extends LDViewHolder> extends RecyclerView.Adapter<T> {
    
    protected abstract T createViewHolder(ViewGroup parent, View v);
    protected abstract int getLayoutId(int viewType);

    protected Context context;

    public LDAdapter(Context context) {
        this.context = context;
    }

    private View getView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(context).inflate(getLayoutId(viewType), parent, false);
    }
    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = getView(parent, viewType);
        return createViewHolder(parent, v);
    }
}
