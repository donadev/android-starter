package it.luigidonadel.appcompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by donadev on 20/12/2018.
 */

public class LDSpeechRecognitionButton extends android.support.v7.widget.AppCompatImageButton implements RecognitionListener {

    public LDSpeechRecognitionButton(Context context) {
        super(context);
    }

    public LDSpeechRecognitionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LDSpeechRecognitionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private final int PERMISSION_RECORD_AUDIO = 45345;
    public void checkPermissions(Activity activity) {

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSION_RECORD_AUDIO);
        }
    }

    private SpeechRecognizer speech;
    private Animation currentAnimation;

    @Override
    public boolean performClick() {
        stopSpeech();
        if(speech == null) speech = SpeechRecognizer.createSpeechRecognizer(getContext());
        speech.setRecognitionListener(this);
        Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "it");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                getContext().getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "it-IT");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        speech.startListening(recognizerIntent);
        currentAnimation = scale(1, 1.2f);
        startAnimation(currentAnimation);
        return super.performClick();
    }
    private Animation scale(float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(800);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.REVERSE);
        return anim;
    }
    private ValueCallback<String> speechResultListener;
    public void setOnSpeechResultListener(ValueCallback<String> listener) {
        this.speechResultListener = listener;
    }
    public void stopSpeech() {
        if(speech != null) {
            speech.cancel();
            speech = null;
        }
        stopAnimation();
    }

    @Override
    public void onReadyForSpeech(Bundle params) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {
        stopSpeech();
    }

    private void stopAnimation() {
        if(currentAnimation != null) {
            currentAnimation.cancel();
            currentAnimation.reset();
        }
        currentAnimation = null;
    }

    @Override
    public void onError(int error) {
        String textError = "Non è stato possibile avviare la dettatura: errore " + error;
        if(error == SpeechRecognizer.ERROR_NO_MATCH) {
            textError = "Nessun risultato trovato";
        } else if(error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
            textError = "Timeout, ripremi il microfono per parlare di nuovo!";
        }
        Toast.makeText(getContext(), textError, Toast.LENGTH_SHORT).show();
        stopSpeech();

    }

    @Override
    public void onResults(Bundle results) {
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        Log.i("Speech", "Found " + matches.size() + " matches");
        if(speechResultListener != null && !matches.isEmpty()) speechResultListener.onReceiveValue(matches.get(0));
        stopSpeech();
    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }
}
