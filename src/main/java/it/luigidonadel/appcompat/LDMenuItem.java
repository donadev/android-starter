package it.luigidonadel.appcompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by donadev on 12/10/2018.
 */

public class LDMenuItem {
    public int id;
    public int name;
    public Boolean arrow = false;
    public Integer icon;
    public MenuType type = MenuType.BIG_TEXT;
    public Boolean selected;

    public LDMenuItem(int id, int name, Boolean arrow, Integer icon, MenuType type) {
        this.id = id;
        this.name = name;
        this.arrow = arrow;
        this.icon = icon;
        this.type = type;
    }
    public LDMenuItem(int id, int name) {
        this.id = id;
        this.name = name;
    }

    public enum MenuType {
        BIG_TEXT, SMALL_TEXT
    }

    private static List<LDMenuItem> left;
    private static List<LDMenuItem> right;
    private static LDMenuItem home;


    public static LDMenuItem getHome() {
        return home;
    }
    public static void setLeft(List<LDMenuItem> input) {
        left = input;
    }
    public static void setRight(List<LDMenuItem> input) {
        right = input;
    }
    public static void setHome(LDMenuItem input) {
        home = input;
    }

    public static List<LDMenuItem> getLeft() {
        return left;
    }
    public static LDMenuItem findById(int id, Boolean left) {
        List<LDMenuItem> items = left ? getLeft() : getRight();
        for(LDMenuItem item : items) {
            if(item.id == id) return item;
        }
        return null;
    }
    public static List<LDMenuItem> getRight() {
        return right;
    }
}
