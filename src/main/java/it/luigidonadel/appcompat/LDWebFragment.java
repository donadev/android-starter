package it.luigidonadel.appcompat;

import android.view.View;
import android.webkit.WebView;

import it.luigidonadel.ldmodules.R;
import it.luigidonadel.utils.AssetUtils;

/**
 * Created by donadev on 26/10/2018.
 */

public abstract class LDWebFragment extends LDFragment {
    WebView contentView;


    public LDWebFragment() {
        // Required empty public constructor
    }


    @Override
    protected void configureView(View v) {
        contentView = v.findViewById(getResources().getIdentifier("webView", "id", getContext().getPackageName()));
        String url = AssetUtils.get(getAssetFileDir(), getAssetFileName());
        if(url != null) contentView.loadUrl(url);
    }
    public abstract String getAssetFileName();
    public abstract String getAssetFileDir();
}
