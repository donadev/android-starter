package it.luigidonadel.appcompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.TextView;

import butterknife.ButterKnife;
import it.luigidonadel.ldmodules.R;

/**
 * Created by donadev on 09/10/2018.
 */

public abstract class LDActivity extends AppCompatActivity implements ValueCallback<LDMenuItem> {

    protected DrawerLayout drawer;
    protected Toolbar toolbar;
    protected LDNavigationLayout userView;

    TextView toolbarTitle;

    private void bindViews() {
        String referencePackage = getPackageName();
        drawer = findViewById(getResources().getIdentifier("drawer_layout", "id", referencePackage));
        toolbar = findViewById(getResources().getIdentifier("toolbar", "id", referencePackage));
        userView = findViewById(getResources().getIdentifier("nav_user", "id", referencePackage));
    }

    public ViewGroup getRootView() {
       return (ViewGroup) findViewById(android.R.id.content);
    }

    @Override
    public void setTitle(int titleId) {
        setTitle(getString(titleId));
    }

    public void setToolbarTint(int color, int titleColor) {
        if(toolbarTitle != null) toolbarTitle.setTextColor(titleColor);
        if(toolbar != null) toolbar.setBackgroundColor(color);
    }
    protected abstract int getColorPrimary();
    protected abstract int getColorPrimaryDark();
    public void setDefaultToolbarTint() {
        setToolbarTint(ContextCompat.getColor(this, getColorPrimary()), ContextCompat.getColor(this, getColorPrimaryDark()));
    }
    public void setToolbarTint(int color) {
        setToolbarTint(color, Color.WHITE);
    }

    protected static boolean foreground = false;

    public static Boolean isAppInForeground() {
        return foreground;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        foreground = true;
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        foreground = false;
    }

    @Override
    public void setTitle(CharSequence title) {
        if(toolbarTitle != null) this.toolbarTitle.setText(title);
        String customTitle = (toolbarTitle != null) ? "" : title.toString();
        if(toolbar != null) toolbar.setTitle(customTitle);
        super.setTitle(customTitle);
    }

    @Override
    public void onBackPressed() {
        if(drawer == null) super.onBackPressed();
        else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    protected Integer getHomeIcon() {
        return null;
    };

    protected abstract Boolean isHome();

    protected Integer getUserDrawerMenuItem() {
        return null;
    }

    private ActionBarDrawerToggle toggle;

    @Override
    public void setContentView(int view) {
        super.setContentView(view);
        ButterKnife.bind(this);
        bindViews();
        if(toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            Integer homeIcon = getHomeIcon();
            if(homeIcon != null) {
                toolbar.setNavigationIcon(homeIcon);
            } else if(drawer != null) {
                toggle = new ActionBarDrawerToggle(this, drawer, R.string.action_drawer_toggle_open, R.string.action_drawer_toggle_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();
            }
            if(!isHome()) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
            }
        }
        if(userView != null) {
            userView.init();
            userView.setValues(LDMenuItem.getRight(), this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Integer userItem = getUserDrawerMenuItem();
        if(toggle != null && toggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (userItem != null && userItem == id) {
            if(drawer != null) drawer.openDrawer(GravityCompat.END);
            return true;
        }
        if(getHomeIcon() != null && id == android.R.id.home) {
            if(drawer != null) drawer.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
