package it.luigidonadel.appcompat;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by donadev on 30/10/2018.
 */

public class LDMenuHolder extends LDViewHolder<LDMenuItem> {
    ImageView iconView;
    public TextView textView;
    ImageView arrowView;

    private SelectHolderCallback<LDMenuItem> holderCallback = (holder, isSelected) -> {
        if(isSelected) itemView.setBackground(getColor(android.R.attr.colorControlHighlight));
        else itemView.setBackground(getColor(android.R.attr.selectableItemBackground));
    };

    public void setSelectGUIHandler(SelectHolderCallback<LDMenuItem> holderCallback) {
        this.holderCallback = holderCallback;
    }
    public LDMenuHolder(View itemView) {
        super(itemView);
        Context context = itemView.getContext();
        iconView = itemView.findViewById(context.getResources().getIdentifier("iconView", "id", context.getPackageName()));
        textView = itemView.findViewById(context.getResources().getIdentifier("textView", "id", context.getPackageName()));
        arrowView = itemView.findViewById(context.getResources().getIdentifier("arrowView", "id", context.getPackageName()));

    }

    private Drawable getColor(int attr) {
        int[] attrs = new int[] { attr /* index 0 */};

        TypedArray ta = context.obtainStyledAttributes(attrs);
        Drawable drawableFromTheme = ta.getDrawable(0 /* index */);

        // Finally free resources used by TypedArray
        ta.recycle();
        return drawableFromTheme;
    }
    @Override
    public void bind(LDMenuItem element) {
        holderCallback.onChangeSelection(this, element.selected);
        iconView.setVisibility(element.icon != null ? VISIBLE : GONE);
        if(element.icon != null) Picasso.get().load(element.icon).into(iconView);
        textView.setText(element.name);
        arrowView.setVisibility(element.arrow ? VISIBLE : INVISIBLE);
        textView.setTextSize(element.type == LDMenuItem.MenuType.SMALL_TEXT ? 11 : 15);
    }

    public interface SelectHolderCallback<T> {
        void onChangeSelection(LDViewHolder<T> holder, Boolean isSelected);
    }
}
