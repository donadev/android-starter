package it.luigidonadel.appcompat;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import it.luigidonadel.ldmodules.R;

/**
 * Created by donadev on 12/10/2018.
 */

public abstract class LDNavigationLayout extends RelativeLayout implements LDMenuHolder.SelectHolderCallback<LDMenuItem> {
    protected ValueCallback<LDMenuItem> callback;

    private RecyclerView menuList;
    private LDNavigationViewAdapter mAdapter;

    public LDNavigationLayout(Context context) {
        super(context);
    }

    public void init() {
        setBackgroundResource(android.R.color.background_light);
        menuList = findViewById(getResources().getIdentifier("menuList", "id", getContext().getPackageName()));
        findViewById(getResources().getIdentifier("headerView", "id", getContext().getPackageName())).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(callback != null) {
                    callback.onReceiveValue(LDMenuItem.getHome());
                    mAdapter.unselectAll();
                }
            }
        });
    }

    public void setSelection(LDMenuItem item) {
        mAdapter.select(item);
    }
    public LDNavigationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LDNavigationLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void setValues(List<LDMenuItem> items, final ValueCallback<LDMenuItem> clickCallback) {
        this.callback = clickCallback;
        mAdapter = new LDNavigationViewAdapter(getContext(), items, getCellLayoutId());
        mAdapter.setSelectCallback(callback, this);
        menuList.setAdapter(mAdapter);
    }
    protected abstract int getCellLayoutId();
    public static class LDNavigationViewAdapter extends LDAdapter<LDViewHolder> {

        private List<LDMenuItem> items;
        private ValueCallback<LDMenuItem> callback;
        private LDMenuHolder.SelectHolderCallback<LDMenuItem> selectGUICallback;
        private Integer selectedMenuIndex = null;
        private int layout_id;
        public LDNavigationViewAdapter(Context context, List<LDMenuItem> items, int layout_id) {
            super(context);
            this.items = items;
            this.layout_id = layout_id;
        }
        public void setSelectCallback(ValueCallback<LDMenuItem> callback, LDMenuHolder.SelectHolderCallback<LDMenuItem> guiHandler) {
            this.callback = callback;
            this.selectGUICallback = guiHandler;
        }
        public LDMenuItem get(int index) {
            return items.get(index);
        }

        private int indexOf(int id) {
            for(int n = 0; n < getItemCount(); n++) {
                if(get(n).id == id) return n;
            }
            return -1;
        }
        public void unselectAll() {
            this.selectedMenuIndex = null;
            this.notifyDataSetChanged();
        }
        public void select(LDMenuItem item) {
            this.selectedMenuIndex = indexOf(item.id);
            this.notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final LDViewHolder holder, final int position) {
            final LDMenuItem item = this.items.get(position);
            item.selected = selectedMenuIndex != null && (selectedMenuIndex == position);
            holder.bind(item);
            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    selectedMenuIndex = holder.getAdapterPosition();
                    callback.onReceiveValue(item);
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        protected LDViewHolder createViewHolder(ViewGroup parent, View v) {
            LDMenuHolder holder = new LDMenuHolder(v);
            holder.setSelectGUIHandler(selectGUICallback);
            return holder;
        }

        @Override
        protected int getLayoutId(int viewType) {
            return layout_id;
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }
}
