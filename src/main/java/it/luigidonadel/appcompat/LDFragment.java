package it.luigidonadel.appcompat;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;

import butterknife.ButterKnife;
import it.luigidonadel.utils.CalendarUtils;

/**
 * Created by donadev on 09/10/2018.
 */

public abstract class LDFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    protected abstract void configureView(View v);
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, v);
        configureView(v);
        return v;
    }
    public Boolean needsWhiteMenu() {
        return false;
    }
    public abstract int getLayoutId();

    protected DatePickerDialog getDatePicker() {
        return new DatePickerDialog(getContext(), this, CalendarUtils.getCurrent(Calendar.YEAR), CalendarUtils.getCurrent(Calendar.MONTH), CalendarUtils.getCurrent(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    protected abstract void handleError(Throwable throwable);
    private void handleGenericError(Throwable error) {
        handleError(error.getLocalizedMessage());
    }
    private void handleError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }
}
