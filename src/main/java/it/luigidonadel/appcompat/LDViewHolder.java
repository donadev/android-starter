package it.luigidonadel.appcompat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.ValueCallback;

import butterknife.ButterKnife;

/**
 * Created by donadev on 09/10/2018.
 */

public abstract class LDViewHolder<T> extends RecyclerView.ViewHolder {
    protected Context context;
    public LDViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }
    public abstract void bind(T element);


    public void setOnSelectListener(final T value, final ValueCallback<T> selectListener) {
        itemView.setOnClickListener(v -> {
            if(selectListener != null) selectListener.onReceiveValue(value);
        });
    }
}
