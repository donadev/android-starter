package it.luigidonadel.callbacks;
/**
 * Created by donadev on 23/10/2018.
 */

public interface ChoiceCallback<T> {
    Boolean choose(T item);
}
