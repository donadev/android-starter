package it.luigidonadel.utils;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by donadev on 30/10/2018.
 */

public class CalendarUtils {

    public static int get(Date date, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(field);
    }
    public static int getCurrent(int field) {
        return get(new Date(), field);
    }
    public static Date getFrom(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }
    public static String simpleFormat(Date date) {
        DateFormat format = DateFormat.getDateInstance();
        return format.format(date);
    }
    public static String simpleDateTimeFormat(Date date) {
        DateFormat format = DateFormat.getDateTimeInstance();
        return format.format(date);
    }
    public static String simpleTimeFormat(Date date) {
        DateFormat format = DateFormat.getTimeInstance();
        return format.format(date);
    }
    public static String simpleDateTimeFormat(long seconds) {
        return simpleDateTimeFormat(new Date(seconds * 1000));
    }
    public static String simpleTimeFormat(long seconds) {
        return simpleTimeFormat(new Date(seconds * 1000));
    }
    public static String simpleFormat(long seconds) {
        return simpleFormat(new Date(seconds * 1000));
    }
}
