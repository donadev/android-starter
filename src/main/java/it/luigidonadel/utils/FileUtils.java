package it.luigidonadel.utils;

import android.content.res.AssetManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by donadev on 17/12/2018.
 */

public class FileUtils {

    public static Boolean containsWord(RandomAccessFile file, String target, int WORD_LEN, int LINE_BREAK_LEN) throws IOException {
        long length = file.length();
        final int size = (int) ((length) / (WORD_LEN + LINE_BREAK_LEN));
        return Collections.binarySearch(
                new AbstractList<String>() {
                    public String get(int pIdx) {
                        try {
                            file.seek((WORD_LEN + LINE_BREAK_LEN) * pIdx);
                            String word = file.readLine().trim();
                            return word;
                        } catch (IOException ex) {
                            throw new RuntimeException(ex);
                        }
                    }

                    public int size() {
                        return size;
                    }
                },
                target,
                (o1, o2) -> o1.toLowerCase().compareTo(o2.toLowerCase())) > 0;
    }
}
