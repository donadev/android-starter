package it.luigidonadel.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.gson.Gson;

/**
 * Created by donadev on 11/11/2018.
 */

public class IntentUtils {

    public static<T> Intent intentFor(Context context, Class destination, T item, String key) {
        Intent i = new Intent(context, destination);
        i.putExtra(key, new Gson().toJson(item));
        return i;
    }
    public static<T> T extractFrom(Intent intent, String key, Class<T> mClass) {
        String raw = intent.getStringExtra(key);
        return new Gson().fromJson(raw, mClass);
    }
    public static<T> T extractFrom(Activity activity, String key, Class<T> mClass) {
        return extractFrom(activity.getIntent(), key, mClass);
    }
    public static Intent visitUrl(String url) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }
}
