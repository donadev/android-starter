package it.luigidonadel.utils;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import static android.content.res.AssetManager.ACCESS_STREAMING;

/**
 * Created by donadev on 22/10/2018.
 */

public class AssetUtils {

    public static String getFolder() {
        return "file:///android_asset/";
    }
    public static String get(String name) {
        return getFolder() + name;
    }
    public static String get(String dir, String name) {
        if(dir == null || name == null) return null;
        return get(dir) + "/" + name;
    }

    public static RandomAccessFile get(Context context, String dirName, String name) throws IOException {
        String path = dirName + "/" + name;
        File outputDir = context.getCacheDir();
        File dirFile = new File(outputDir, dirName);
        dirFile.mkdirs();
        File destFile = new File(dirFile, name);
        if(!destFile.exists()) {
            destFile.createNewFile();
            OutputStream dest = new FileOutputStream(destFile, true);
            InputStream src = context.getAssets().open(path, ACCESS_STREAMING);
            byte[] buff = new byte[100 * 1024];
            for (; ; ) {
                int cnt = src.read(buff);
                if (cnt <= 0)
                    break;
                dest.write(buff, 0, cnt);
            }
            dest.flush();
            dest.close();
        }
        return new RandomAccessFile(destFile, "r");
    }
}
