package it.luigidonadel.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by donadev on 13/11/2018.
 */

public class ArrayUtils {
    public static<T> void reverse(T[] items) {
        T[] copy = items.clone();
        for(int n = 0; n < items.length; n++) {
            items[n] = copy[items.length - 1 - n];
        }
    }
    public static void shuffle(Character[][] items, int width, int height) {
        for(int i = width * height - 1; i >= 1; i--) {
            int j = new Random().nextInt(i + 1);
            char temp = items[j / height][j % height];
            items[j / height][j % height] = items[i / height][i % height];
            items[i / height][i % height] = temp;
        }
    }
    public static void shuffle(Integer[] items) {
        List<Integer> output = Arrays.asList(items);
        Collections.shuffle(output);
        items = new Integer[items.length];
        output.toArray(items);
    }
    public static void shuffle(Character[] items) {
        List<Character> output = Arrays.asList(items);
        Collections.shuffle(output);
        items = new Character[items.length];
        output.toArray(items);
    }
    public static void shuffle(Boolean[] items) {
        List<Boolean> output = Arrays.asList(items);
        Collections.shuffle(output);
        items = new Boolean[items.length];
        output.toArray(items);
    }
    public static void shuffle(Integer[][] items) {
        List<Integer[]> output = Arrays.asList(items);
        Collections.shuffle(output);
        items = new Integer[items.length][];
        output.toArray(items);
    }
    public static void shuffle(String[] items) {
        List<String> output = Arrays.asList(items);
        Collections.shuffle(output);
        items = new String[items.length];
        output.toArray(items);
    }

    public static void reverse(char[] items) {
        char[] copy = items.clone();
        for(int n = 0; n < items.length; n++) {
            items[n] = copy[items.length - 1 - n];
        }
    }
}
