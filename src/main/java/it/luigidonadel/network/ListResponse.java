package it.luigidonadel.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by donadev on 11/11/2018.
 */

public class ListResponse<T> extends Response {

    @SerializedName("info") @Expose private List<T> items;

    public ListResponse() {}

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
