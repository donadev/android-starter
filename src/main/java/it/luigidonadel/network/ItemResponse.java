package it.luigidonadel.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by donadev on 11/11/2018.
 */

public class ItemResponse<T> extends Response {

    @SerializedName("info") @Expose private T item;

    public ItemResponse() {}


    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
